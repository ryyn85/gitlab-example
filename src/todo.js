/**
 * Created by merh on 24.03.17.
 */
const todos = [];


class Todo {
    constructor(title, description) {
        this.title = title;
        this.description = description;
    }
}

function addTodo(todo) {
    todos.push(todo);
}

function addTodoView(todo) {
    const view = document.getElementById('view');
    const wrapper = document.createElement('DIV');
    const title = document.createElement('H3');
    const description = document.createElement('P');

    title.appendChild(document.createTextNode(todo.title));
    description.appendChild(document.createTextNode(todo.description));
    wrapper.appendChild(title);
    wrapper.appendChild(description);
    wrapper.className = 'todo-container box box-sizing';
    view.appendChild(wrapper);
}

function submitTodo() {
    const title = document.getElementById('title');
    const description = document.getElementById('description');
    const todo = new Todo(title.value, description.value);

    addTodo(todo);
    addTodoView(todo);

    title.value = '';
    description.value = '';
}